//
//  ViewController.swift
//  Task4
//
//  Created by Евгений Григоренко on 4.10.21.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController, MKMapViewDelegate {
    
    let get = GetMarkerViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(get.mapView)
        location()
        self.constraint()
        get.getMarker()
    }
    
    func location(_ location: CLLocation = CLLocation(latitude: 52.425163, longitude: 31.015039), locationRadius: CLLocationDistance = 1500){
        let coordinateRegion = MKCoordinateRegion.init(center: location.coordinate,
                                                      latitudinalMeters: locationRadius,
                                                      longitudinalMeters: locationRadius)
        get.mapView.setRegion(coordinateRegion, animated: true)
    }
    func constraint(){
        get.mapView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        get.mapView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        get.mapView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        get.mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
}


