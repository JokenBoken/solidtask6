//
//  Infokioski.swift
//  Task4
//
//  Created by Евгений Григоренко on 17.10.21.
//

import Foundation

struct Infokiosk: Codable {
    let info_id: Int
    let city: String
    let address: String
    let gps_x: String
    let gps_y: String
}
