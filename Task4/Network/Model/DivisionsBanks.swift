//
//  DivisionsBanks.swift
//  Task4
//
//  Created by Евгений Григоренко on 18.10.21.
//

import Foundation

struct DivisionsBanks: Codable {
    let filial_id: String
    let street: String
    let name: String
    let GPS_X: String
    let GPS_Y: String
}
