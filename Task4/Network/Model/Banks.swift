//
//  Banks.swift
//  Task4
//
//  Created by Евгений Григоренко on 18.10.21.
//

import Foundation

struct ATMs: Codable {
    let id: String
    let city: String
    let address: String
    let gps_x: String
    let gps_y: String
}

