//
//  DataFetcherService.swift
//  Task4
//
//  Created by Евгений Григоренко on 18.10.21.
//

import Foundation
protocol Fetcher {
    func fetchIfokiosk(completion: @escaping ([Infokiosk]?) -> Void)
    func fetchATM(completion: @escaping ([ATMs]?) -> Void)
    func fetchDivisionsBanks(completion: @escaping ([DivisionsBanks]?) -> Void) 

}

class DataFetcherService: Fetcher {
    var dataFetcher: DataFetcher
    
    init(dataFetcher: DataFetcher = NetworkDataFetcher()) {
        self.dataFetcher = dataFetcher
    }
    
    func fetchIfokiosk(completion: @escaping ([Infokiosk]?) -> Void) {
        let urlString = "https://belarusbank.by/api/infobox"
        dataFetcher.fetchGenericJSONData(urlString: urlString, response: completion)        
    }
       
    func fetchATM(completion: @escaping ([ATMs]?) -> Void) {
        let urlString = "https://belarusbank.by/api/atm"
        dataFetcher.fetchGenericJSONData(urlString: urlString, response: completion)
    }
    func fetchDivisionsBanks(completion: @escaping ([DivisionsBanks]?) -> Void) {
        let urlString = "https://belarusbank.by/api/filials_info"
        dataFetcher.fetchGenericJSONData(urlString: urlString, response: completion)
    }
}
