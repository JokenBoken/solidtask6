//
//  GetMarker.swift
//  Task4
//
//  Created by Евгений Григоренко on 23.10.21.
//

import Foundation
import MapKit

protocol Markers {
    func getMarker() -> [Marker]
}

class GetMarkerViewModel {
    let dataFetcherService = DataFetcherService()
    var mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.translatesAutoresizingMaskIntoConstraints = false
        return mapView
    }()
    func getMarker() {
        var markers: [Marker] = []
        let n = 10
        
        dataFetcherService.fetchIfokiosk {(infokiosk) in
            let infokiosksGomel = infokiosk?.filter{i in i.city == "Гомель"}
            for kiosk in infokiosksGomel ?? [] {
                guard let doubleX = Double(kiosk.gps_x) else { return }
                guard let doubleY = Double(kiosk.gps_y) else { return }
                let stringID = String(kiosk.info_id as Int)
                let location = CLLocation(latitude : doubleX, longitude: doubleY)
                let staticLocation = CLLocation(latitude: 52.425163, longitude: 31.015039)
                let distance = location.distance(from: staticLocation)
                markers.append(Marker.init(id: stringID, distance: distance, adress: kiosk.address, x: doubleX, y: doubleY))
            }
        }
        
        dataFetcherService.fetchATM{ (ATM) in
            let ATMsGomel = ATM?.filter{i in i.city == "Гомель"}
            for ATM in ATMsGomel ?? [] {
                guard let doubleX = Double(ATM.gps_x) else { return }
                guard let doubleY = Double(ATM.gps_y) else { return }
                let stringID = ATM.id
                let location = CLLocation(latitude : doubleX, longitude: doubleY)
                let staticLocation = CLLocation(latitude: 52.425163, longitude: 31.015039)
                let distance = location.distance(from: staticLocation)
                markers.append(Marker.init(id: stringID, distance: distance, adress: ATM.address, x: doubleX, y: doubleY))
            }
        }
        
        dataFetcherService.fetchDivisionsBanks{ (DivisionsBanks) in
            let DivBanks = DivisionsBanks?.filter{i in i.name == "Гомель"}
            for bank in DivBanks ?? [] {
                guard let doubleX = Double(bank.GPS_X) else { return }
                guard let doubleY = Double(bank.GPS_Y) else { return }
                let stringID = bank.filial_id
                let location = CLLocation(latitude : doubleX, longitude: doubleY)
                let staticLocation = CLLocation(latitude: 52.425163, longitude: 31.015039)
                let distance = location.distance(from: staticLocation)
                markers.append(Marker.init(id: stringID, distance: distance, adress: bank.street, x: doubleX, y: doubleY))
            }
            var annotations: [MKPointAnnotation] = []
                        for marker in markers[0...n] {
                            print(marker)
                            let annotation = MKPointAnnotation()
                            annotation.title = marker.adress
                            annotation.coordinate = CLLocationCoordinate2D(latitude : marker.x, longitude: marker.y)
                            annotations.append(annotation)
                        }
            DispatchQueue.main.async { self.mapView.addAnnotations(annotations) }

        }
    }
    
}
