//
//  Marker.swift
//  Task4
//
//  Created by Евгений Григоренко on 18.10.21.
//

import Foundation
import MapKit

struct Marker {
    let id: String
    let distance: CLLocationDistance
    let adress: String
    let x: Double
    let y: Double
}
